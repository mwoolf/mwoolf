//
//  LetterStatus.swift
//  wordy
//
//  Created by Max Woolf on 11/01/2022.
//

import Foundation

enum LetterStatus {
    case correct
    case nonExistant
    case wrongLocation
}
