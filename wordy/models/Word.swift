//
//  Word.swift
//  wordy
//
//  Created by Max Woolf on 11/01/2022.
//

import Foundation

struct Word {
    let word: String
    
    var letters: [String] {
        get {
            self.word.map { String($0).uppercased() }
        }
    }
}
