## Hey, I'm Max! 👋

I'm a Staff Engineer in the Monitor stage, focussing on Analytics and Observability. This README should help you learn about me and the ways I work and how best to work with me.

## About me

I've been a _professional_ software engineer since 2012 and an amateur software tinkerer since some point in the 1990s.

During my professional career I've worked for companies of all shapes and sizes, as well as myself. My career has led me to join GitLab in April 2020 and I'm really glad it did.

I was born, raised and still live in [Bournville](https://en.wikipedia.org/wiki/Bournville), England. It's about 90 minutes north of London, _when the trains are running on time_. \
When I'm not working, I spend lots of time in my local community as the vice-chair of [Friends of Cotteridge Park](https://cotteridgepark.org.uk), [a governor](https://governorsforschools.org.uk/about-the-role/) at my local primary school or volunteering with junior developers in bootcamps to help them start a career that's brought me so much joy.

 I'm married to my wife, Ciara and have [two dogs](https://www.instagram.com/willow_and_winston_woolf/), [Winston](https://about.gitlab.com/company/team-pets/#243-winston) and [Willow](https://about.gitlab.com/company/team-pets/#315-willow). (I talk about them a lot.) I try to spend as much time as possible with them away from my computer when I'm not working; usually hiking up a mountain.

## What I do

I'm a Staff Backend Engineer which means my days are usually made up of:

* Feature development
* Feature planning
* Code reviews
* General team collaboration
* Community contribution triage and collaboration
* Posting cute pictures of my dogs in the #dog Slack channel

I'm also a backend maintainer of the main GitLab Rails repository and a database reviewer.

## How I work

Getting hold of me is easy. I work as asyncronously as possible, but I'm usually near my computer between 8am and 5pm UK time if you need to get hold of me quickly.

### For work related enquiries:

**Urgent? Ping me on Slack in a public channel.** Avoid DMs unless confidential or otherwise unavoidable. I'll politely ask you to ask the question again in a public channel or issue if its appropriate to do so. (This may come across as rude, but it's to make sure that everyone can contribute.)

**Not urgent? Ping me in an issue or merge request.** I triage my todos at the start of every working day, so this is the most effective way to get hold of me. My emails are often marked as read if I know that there's a corresponding todo waiting for me.

### For non-work/informal/fun stuff

Ping me on Slack. I'm not a robot. 🤖 I always want to see a cake you baked, a picture of your dogs or just a cool thing you built. 

☕️ Coffee chats are **always welcome.**
