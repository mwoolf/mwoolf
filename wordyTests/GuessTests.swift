//
//  GuessTests.swift
//  wordyTests
//
//  Created by Max Woolf on 11/01/2022.
//

import XCTest

class GuessTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testInvalidWord() throws {
        let word = Word(word: "hello")
        let guess = Guess(word: word, submittedGuess: "avdpw")
        
        XCTAssertFalse(guess.isValid)
    }
    
    func testValidWord() throws {
        let word = Word(word: "hello")
        let guess = Guess(word: word, submittedGuess: "hello")
        
        XCTAssertTrue(guess.isValid)
    }
    
    func testValidWordCaseInsensitive() throws {
        let word = Word(word: "HeLlO")
        let guess = Guess(word: word, submittedGuess: "hEllO")
        
        XCTAssertTrue(guess.isValid)
    }
    
    func testPartiallyValidWord() throws {
        let word = Word(word: "Hello")
        let guess = Guess(word: word, submittedGuess: "hovls")
        
        XCTAssertEqual(guess.result, [.correct, .wrongLocation, .nonExistant, .correct, .nonExistant])
    }
}
